
#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Can't scan a directory")]
    Dir(#[from] #[source] walkdir::Error),
    #[error("I/O error")]
    IO(#[from] #[source] std::io::Error),
}
