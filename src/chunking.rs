use std::io::SeekFrom;
use std::fs::File;
use std::io::Read;
use std::io::Seek;
use std::path::Path;
use rangemap::RangeMap;
use rangemap::RangeSet;
use std::collections::HashSet;
use std::ops::Range;
use std::collections::HashMap;
use std::sync::Mutex;
use std::fs::FileType;
use rayon::prelude::*;
use hash_roll::Chunk;
use std::path::PathBuf;

use walkdir::WalkDir;
use crate::error::Error;

pub struct AFile {
    rel_path: PathBuf,
    ty: FileType,
    len: usize,
}

pub struct ChunkedArchive {
    pub root: PathBuf,
    pub files: Vec<AFile>,
    pub chunks: FileChunkMap,
}

pub fn load_file_range(path: &Path, range: Range<usize>) -> Result<Vec<u8>, Error> {
    let mut f = File::open(path)?;
    f.seek(SeekFrom::Start(range.start as _))?;
    let mut out = Vec::with_capacity(range.len());
    f.take(range.len() as _).read_to_end(&mut out)?;
    Ok(out)
}

pub fn scan_root_directories(root_paths: Vec<PathBuf>) -> Result<Vec<ChunkedArchive>, Error> {
    let results: Vec<_> = root_paths.into_par_iter().map(|root| {
        let root = std::fs::canonicalize(&root).unwrap_or(root);
        let rel_paths = WalkDir::new(&root).into_iter()
            .filter_map(|e| e.map(|e| {
                let ty = e.file_type();
                if ty.is_dir() {
                    return None;
                }
                let path = e.path().strip_prefix(&root).expect("bad prefix!?");
                Some((ty, path.to_owned()))
            }).transpose())
            .collect::<Result<Vec<_>, walkdir::Error>>()?;

        let (chunks, files) = compute_chunks(&root, rel_paths)?;
        Ok::<_, Error>(ChunkedArchive {
            root, files, chunks
        })
    }).collect();
    results.into_iter().collect()
}

fn get_final_file_index<'a>(index: &Mutex<Vec<&'a Path>>, path: &'a Path) -> usize {
    let mut index = index.lock().unwrap();
    if let Some(found) = index.iter().position(|&p| p == path) {
        return found;
    }
    let new_idx = index.len();
    index.push(path);
    new_idx
}

#[derive(Eq, PartialEq)]
pub struct TempRange {
    pub target: Range<usize>,
    pub source: Range<usize>,
    // for final combined set in the created archive
    pub source_file_index: usize,
    // to spot when copying whole file
    pub source_file_len: usize,
    pub fake_data: bool,
}

pub struct MappedArchive {
    pub file_names: Vec<PathBuf>,
    pub file_ranges: Vec<MappedFileRanges>,
}

pub struct MappedFileRanges {
    pub target_file_name: usize,
    pub by_archive: Vec<Vec<TempRange>>,
}

pub fn finalize_file_ranges(target: &ChunkedArchive, set: &Set) -> MappedArchive {
    let mut final_file_indices: Vec<_> = target.files.iter().map(|f| f.rel_path.as_path()).collect();
    final_file_indices.sort();
    let final_file_indices = Mutex::new(final_file_indices);

    let target_file_hash_ranges = map_chunks_to_ranges(target, set);

    let all_ranges = target_file_hash_ranges.iter().enumerate().filter_map(|(archive_target_file_idx, ranges)| {
        if !target.files[archive_target_file_idx].ty.is_file() || target.files[archive_target_file_idx].len == 0 {
            return None;
        }
        assert!(target.files[archive_target_file_idx].len > 0);
        let target_rel_path = &target.files[archive_target_file_idx].rel_path;
        let target_file_index = get_final_file_index(&final_file_indices, target_rel_path);
        println!("{} is made of {}B: ", target_rel_path.display(), target.files[archive_target_file_idx].len);
        let mut all_ranges: Vec<_> = set.archives.iter().map(|src_archive| {
            let mut fin_ranges: Vec<TempRange> = Vec::new();
            let mut prev_src_file_index = !0;
            for (target_range, hash_sub_slice, hash) in ranges.iter() {
                // pick the same file as a source, or an already-used file (faster decompression), or a largest file (best chance of extending)
                // TODO: maybe just copy the whole set until extending is done?
                // TODO: also look at offset to match contiguous source in case of repeated data in the same file. maybe have a fast case for same file?
                let src_file_chunk = src_archive.chunks[hash].iter().find(|&f| &src_archive.files[f.file_index].rel_path == target_rel_path)
                    .unwrap_or_else(|| src_archive.chunks[hash].iter().find(|&f| f.file_index == prev_src_file_index)
                        .unwrap_or_else(|| src_archive.chunks[hash].iter().max_by_key(|f| src_archive.files[f.file_index].len).unwrap()));
                let src_file_meta = &src_archive.files[src_file_chunk.file_index];
                prev_src_file_index = src_file_chunk.file_index;

                let final_source_file_index = get_final_file_index(&final_file_indices, &src_file_meta.rel_path);
                let mut src_range = src_file_chunk.range.clone();
                assert!(src_range.len() >= hash_sub_slice.len());

                src_range.start += hash_sub_slice.start;
                src_range.end = src_range.start + hash_sub_slice.len();

                assert_eq!(target_range.len(), src_range.len(), "{:?} {:?} {:?}", src_range, hash_sub_slice, target_range);

                match fin_ranges.last_mut() {
                    Some(r) if final_source_file_index == r.source_file_index &&
                        r.target.end == target_range.start &&
                        r.source.end == src_range.start => {
                            r.target.end = target_range.end;
                            r.source.end = src_range.end;
                        },
                    _ => {
                        fin_ranges.push(TempRange {
                            target: target_range.clone(),
                            source: src_range,
                            source_file_index: final_source_file_index,
                            source_file_len: src_file_meta.len,
                            fake_data: false,
                        });
                    }
                }
            }
            fin_ranges
        }).collect();

        extend_ranges(target, archive_target_file_idx, &mut all_ranges, &final_file_indices, set, true).expect("range bad");
        extend_ranges(target, archive_target_file_idx, &mut all_ranges, &final_file_indices, set, false).expect("range bad");
        eprintln!("map1");
        Some(MappedFileRanges {
            target_file_name: target_file_index,
            by_archive: all_ranges,
        })
    }).collect();
    eprintln!("map2");

    MappedArchive {
        file_names: final_file_indices.into_inner().unwrap().into_iter().map(PathBuf::from).collect(),
        file_ranges: all_ranges,
    }
}

fn extend_ranges(target: &ChunkedArchive, archive_target_file_idx: usize, all_ranges: &mut Vec<Vec<TempRange>>, final_file_indices: &Mutex<Vec<&Path>>, set: &Set, match_target: bool) -> Result<(), Error> {
    // so far all ranges came straight from hashes, now widen ranges if possible based on actual file data
    let mut gap_maps = vec![RangeMap::new(); set.archives.len()];
    for (gap_map, ranges) in gap_maps.iter_mut().zip(all_ranges.iter()) {
        for (range_idx, r) in ranges.iter().enumerate() {
            gap_map.insert(r.target.clone(), range_idx);
        }
    }

    let target_path = target.root.join(&target.files[archive_target_file_idx].rel_path);
    let target_len = target.files[archive_target_file_idx].len;

    let mut min_gap_start = 0;
    for mut gap in gap_maps[0].gaps(&(0..target_len)) {
        if gap.len() == 0 {
            continue;
        }

        // FIXME:  there's double overlap, because update of a range doesn't update the next gap!

        let target_file_gap = load_file_range(&target_path, gap.clone())?;


        // extending next range backwards
        if gap.end < target_len {
            assert!(gap.end > 0);

            let files_by_set = gap_maps.iter().enumerate().map(|(set_archive_idx, gap_map)| {
                // get the range before the gap, for this source
                let (_, &range_idx) = gap_map.get_key_value(&gap.end).expect("range must be there");
                let r = &all_ranges[set_archive_idx][range_idx];
                assert_eq!(r.target.start, gap.end);
                let rel_path = final_file_indices.lock().unwrap()[r.source_file_index];
                let path = set.archives[set_archive_idx].root.join(rel_path);


                let range = r.source.start.saturating_sub(gap.len())..r.source.start;
                let data = load_file_range(&path, range)?;
                Ok((data, range_idx))
            }).collect::<Result<Vec<_>, Error>>()?;

            let extend_by = (0..gap.len()).take_while(|&next_byte_offset| {
                let target_byte = if match_target {
                    target_file_gap[target_file_gap.len() - next_byte_offset - 1] // -1 because range is not inclusive
                } else {
                    let src_gap = &files_by_set[0].0;
                    if src_gap.len() < next_byte_offset + 1 {
                        return false;
                    }
                    src_gap[src_gap.len() - next_byte_offset - 1]
                };
                // all sources must have the same byte, and we want it to come from the same source range
                files_by_set.iter().all(|(src_gap, _range_idx)| {
                    if src_gap.len() < next_byte_offset + 1 {
                        return false;
                    }
                    src_gap[src_gap.len() - next_byte_offset - 1] == target_byte
                })
            }).last().map(|n| n+1).unwrap_or(0);
            if extend_by > 0 {
                println!("yay! gap {:?} ({}) can be filled from end by {} {}", gap, gap.len(), extend_by, match_target);
                files_by_set.iter().enumerate().for_each(|(set_archive_idx, (_src, range_idx))| {
                    let r = &mut all_ranges[set_archive_idx][*range_idx];
                    if !match_target {
                        r.fake_data = true;
                    }
                    r.target.start -= extend_by;
                    r.source.start -= extend_by;
                });
                gap.end -= extend_by;
            }
        }

        // extending a range before the gap, forwards
        if gap.start > min_gap_start {
            // FIXME: reading whole files here is ridiculously inefficient
            let files_by_set = gap_maps.iter().enumerate().map(|(set_archive_idx, gap_map)| {
                // get the range before the gap, for this source
                let (_, &range_idx) = gap_map.get_key_value(&(gap.start - 1)).expect("range must be there");
                let r = &all_ranges[set_archive_idx][range_idx];
                assert_eq!(r.target.end, gap.start);
                let rel_path = final_file_indices.lock().unwrap()[r.source_file_index];
                let path = set.archives[set_archive_idx].root.join(rel_path);
                let load_end = (r.source.end + gap.len()).min(r.source_file_len);
                let file_data = load_file_range(&path, r.source.end..load_end)?;
                Ok((file_data, range_idx))
            }).collect::<Result<Vec<_>, Error>>()?;

            let extend_by = (0..gap.len()).take_while(|&next_byte_offset| {
                let target_byte = if match_target {
                    target_file_gap[next_byte_offset]
                } else {
                    let src_gap = &files_by_set[0].0;
                    if src_gap.len() <= next_byte_offset {
                        return false;
                    }
                    src_gap[next_byte_offset]
                };
                // all sources must have the same byte, and we want it to come from the same source range
                files_by_set.iter().all(|(src_gap, _range_idx)| {
                    // previous range end pos = gap start pos, because ranges are not inclusive
                    src_gap.get(next_byte_offset).copied() == Some(target_byte)
                })
            }).last().map(|n| n+1).unwrap_or(0);
            if extend_by > 0 {
                println!("yay! gap {:?} ({}) can be filled from start by {} {}", gap, gap.len(), extend_by, match_target);
                files_by_set.iter().enumerate().for_each(|(set_archive_idx, (_src_file, range_idx))| {
                    let r = &mut all_ranges[set_archive_idx][*range_idx];
                    if !match_target {
                        r.fake_data = true;
                    }
                    r.target.end += extend_by;
                    r.source.end += extend_by;
                });
                min_gap_start = gap.end + extend_by;
            }
        }

        // TODO: fill the gaps with bytes that match all sources, but not necessarily the dest!
    }
    Ok(())
}

fn map_chunks_to_ranges(target: &ChunkedArchive, set: &Set) -> Vec<Vec<(Range<usize>, Range<usize>, [u8; 32])>> {
    let mut target_file_hash_ranges_sets = vec![RangeSet::<usize>::new(); target.files.len()];
    let mut target_file_hash_ranges = vec![Vec::new(); target.files.len()];
    let mut to_insert = Vec::with_capacity(6);

    // TODO: group them by file instead, and then try same-file chunks first
    let mut applicable_chunks: Vec<_> = target.chunks.iter().filter(|&(hash,_)| set.hashes.contains(hash)).collect();
    applicable_chunks.sort_by(|&(_,a), &(_,b)| b[0].range.len().cmp(&a[0].range.len()));

    for (hash, chunks) in applicable_chunks {
        for chunk in chunks {
            let target_range = chunk.range.clone();
            let satisfied_ranges = &mut target_file_hash_ranges_sets[chunk.file_index];
            let ranges = &mut target_file_hash_ranges[chunk.file_index];
            for gap in satisfied_ranges.gaps(&target_range) {
                if gap.len() == 0 {
                    continue;
                }
                if gap == target_range {
                    to_insert.push((target_range.clone(), 0..target_range.len()));
                } else {
                    // taking only a fragment of a hashed slice
                    assert!(gap.len() <= target_range.len());
                    assert!(gap.end <= target_range.end);
                    assert!(gap.start >= target_range.start);
                    let offset_from_start = gap.start - target_range.start;
                    to_insert.push((gap.clone(), offset_from_start..offset_from_start + gap.len()));
                }
            }
            for r in to_insert.drain(..) {
                satisfied_ranges.insert(r.0.clone());
                ranges.push((r.0, r.1, *hash));
            }
        }
    }
    for r in &mut target_file_hash_ranges {
        r.sort_by_key(|r| r.0.start);
    }
    target_file_hash_ranges
}

const MAX_BLOAT_PERCENTAGE: usize = 20;

pub fn take_similar_archives(target: &ChunkedArchive, candidates: Vec<ChunkedArchive>) -> Vec<Set> {
    let mut candidates: Vec<_> = candidates.into_iter().map(|candidate| {
        let missing_bytes = missing_bytes(target, |h| candidate.chunks.get(h).is_some());
        println!("{} lacks {} bytes of {}", candidate.root.display(), missing_bytes, target.root.display());
        (missing_bytes, candidate)
    }).collect();

    candidates.sort_by_key(|c| !c.0); // reverse sort

    let mut sets = Vec::with_capacity(candidates.len()/2);
    while let Some((best_missing_bytes, best_candidate)) = candidates.pop() {
        let max_missing_bytes = best_missing_bytes / 100 * (100 + MAX_BLOAT_PERCENTAGE);

        let mut common_hashes: HashSet<_> = target.chunks.keys().copied().filter(|h| best_candidate.chunks.get(h).is_some()).collect();
        println!("{} starting with {} hashes out of {}", best_candidate.root.display(), common_hashes.len(), target.chunks.len());

        let mut set = Vec::with_capacity(candidates.len()/3);
        set.push(best_candidate);

        // update missing for new hash set
        candidates.iter_mut().for_each(|(miss, candidate)| {
            *miss = missing_bytes(target, |h| common_hashes.contains(h) && candidate.chunks.get(h).is_some());
        });
        candidates.sort_by_key(|c| !c.0); // reverse sort

        while let Some((missing_bytes, candidate)) = candidates.last() {
            println!("{} is missing {}", candidate.root.display(), missing_bytes);
            if *missing_bytes > max_missing_bytes {
                println!("not taking the last one more {}", max_missing_bytes);
                break;
            }
            common_hashes.retain(|h| candidate.chunks.get(h).is_some());
            set.push(candidates.pop().unwrap().1);
        }
        sets.push(Set {
            archives: set,
            hashes: common_hashes,
        });
    }
    sets
}

pub struct Set {
    pub archives: Vec<ChunkedArchive>,
    pub hashes: HashSet<[u8; 32]>,
}

fn missing_bytes(target: &ChunkedArchive, has_hash: impl Fn(&[u8; 32]) -> bool) -> usize {
    let mut file_ranges: Vec<_> = target.files.iter().map(|f| (f.len, RangeSet::new())).collect();
    let common_chunks = target.chunks.iter().filter(|&(hash, _)| has_hash(hash));
    for (_, files) in common_chunks {
        for f in files {
            assert!(f.range.len() > 0);
            file_ranges[f.file_index].1.insert(f.range.clone());
        }
    }
    file_ranges.into_iter().map(|(len, ranges)| {
        // it would be nice to measure entropy of these gaps
        ranges.gaps(&(0..len)).map(|g| g.len()).sum::<usize>()
    }).sum::<usize>()
}

pub struct FileChunk {
    file_index: usize,
    range: Range<usize>,
}

type FileChunkMap = HashMap<[u8; 32], Vec<FileChunk>>;

const MAX_CHUNKS: usize = 100_000;
/// Smaller values dedupe better, but take more RAM to generate,
/// and there's a small risk of creating an inefficient diff.
const NORMAL_CHUNK_SIZE: usize = 400;
/// Lower than this is pure overhead
const MIN_CHUNK_SIZE: usize = 32;

fn compute_chunks(root: &Path, rel_paths: Vec<(FileType, PathBuf)>) -> Result<(FileChunkMap, Vec<AFile>), Error> {
    let hashes = Mutex::new(HashMap::new());
    let files: Vec<_> = rel_paths.into_par_iter().with_min_len(1).enumerate()
        .map(|(file_index, (ty, rel_path))| Ok({
            if ty.is_file() {
                let file_data = std::fs::read(root.join(&rel_path))?;
                if file_data.len() > 0 {
                    // split files into at least 4 chunks, so even small files get some dedupe
                    let chunk_size = NORMAL_CHUNK_SIZE.min(file_data.len() / 4).max(MIN_CHUNK_SIZE.max(file_data.len() / MAX_CHUNKS));
                    // TODO: file-type specific splitting, e.g. line breaks for txt
                    rayon::join(
                        || split_into_hashed_chunks(hash_roll::ram::Ram::with_w(chunk_size as _), file_index, &file_data, &hashes),
                        || split_into_hashed_chunks(hash_roll::bup::RollSum::with_window(chunk_size), file_index, &file_data, &hashes));
                }
                AFile {
                    ty, rel_path,
                    len: file_data.len(),
                }
            } else {
                // symlink
                AFile {
                    ty, rel_path, len: 0,
                }
            }
        })).collect();
    let files = files.into_iter().collect::<Result<_, Error>>()?;
    Ok((hashes.into_inner().unwrap(), files))
}

fn split_into_hashed_chunks(chunk: impl Chunk, file_index: usize, file_data: &[u8], hashes: &Mutex<HashMap<[u8; 32], Vec<FileChunk>>>) {
    let mut ss = chunk.to_search_state();
    let mut data_window = file_data;
    let mut prev_cut = 0;
    loop {
        let (cut, advance_by) = chunk.find_chunk_edge(&mut ss, data_window);

        let cut_rel_offset = cut.unwrap_or(data_window.len());

        let cut_abs_offset = file_data.len() - data_window.len() + cut_rel_offset;
        if cut_abs_offset > prev_cut {
            let range = prev_cut..cut_abs_offset;
            assert!(range.len() > 0);
            let chunk = &file_data[range.clone()];
            let hash = blake3::hash(chunk);
            hashes.lock().unwrap().entry(*hash.as_bytes())
                .or_insert_with(Vec::new)
                .push(FileChunk {
                    file_index,
                    range,
                });
        } else {
            eprintln!("bad range? {:?} {} {}", cut, cut_rel_offset, advance_by);
        }
        prev_cut = cut_abs_offset;

        if cut.is_none() {
            break;
        }

        data_window = &data_window[advance_by..];
        if data_window.is_empty() {
            break;
        }
    }
}
