use crate::chunking::load_file_range;
use std::path::Path;
use rayon::prelude::*;
use std::io::BufWriter;
use std::io::Write;
use std::fs::File;
use std::path::PathBuf;

mod error;
pub use error::*;

mod chunking;
pub use chunking::*;

fn main() -> Result<(), Error> {
    let paths: Vec<_> = std::env::args_os().skip(1).map(PathBuf::from).collect();

    let mut archives = scan_root_directories(paths)?;

    let target = archives.remove(0);

    archives.retain(|a| a.root != target.root); // hack for bad inputs

    let sets = take_similar_archives(&target, archives);
    println!("got {} sets", sets.len());
    for (set_idx, set) in sets.iter().enumerate() {
        println!("set {} covers {} archives", set_idx, set.archives.len());
        for a in &set.archives {
            println!(" - {}", a.root.display());
        }
    }

    sets.par_iter().enumerate().try_for_each(|(set_idx, set)| {
        eprintln!("finalize_file_ranges {}", set_idx);
        let archive = finalize_file_ranges(&target, &set);
        eprintln!("compressing {}", set_idx);

        let out_filename = format!("test-set-{}-z2-{}.zdd", set_idx, ZSTD_LEVEL);
        let mut out_file = BufWriter::new(File::create(out_filename)?);
        out_file.write_all(b"zdd1")?;
        compress_archive(out_file, archive, &target.root, set)
    })?;

    Ok(())
}

fn leb(w: &mut impl Write, val: impl Into<u64>) -> std::io::Result<()> {
    let val = val.into();
    leb128::write::unsigned(w, val)?;
    Ok(())
}

fn compress_archive(mut out: BufWriter<File>, mut archive: MappedArchive, target_root: &Path, set: &Set) -> Result<(), Error> {
    let mut file_names = Vec::new();

    for f in &archive.file_names {
        file_names.extend_from_slice(f.to_str().unwrap().as_bytes());
        file_names.push(0);
    }

    // TODO: write window size?
    // TODO: write map of version -> archive index

    write_zstd_chunk(&mut out, 'N', &file_names, None)?;
    drop(file_names);

    let mut rng_out = Vec::new();
    leb(&mut rng_out, archive.file_ranges.len() as u64)?;
    for file_in_chunks in &mut archive.file_ranges {
        for a in &mut file_in_chunks.by_archive {
            // we don't write the target - zstd picks it
            a.sort_by_key(|r| r.source.start);
        }
    }

    // TODO: order of files is important for a solid archive

    for file_in_chunks in &archive.file_ranges {
        // TODO: symlinks, directories, etc.

        leb(&mut rng_out, file_in_chunks.target_file_name as u64)?;
        let mut by_archive = &file_in_chunks.by_archive[..];
        leb(&mut rng_out, by_archive.len() as u64)?;

        // pop identical ones off the end (dedupe)
        // decoding rule is: if out of bounds, take last
        // TODO: maybe this should be properly uniqued and remapped?
        let last = by_archive.last().expect("wat");
        loop {
            if by_archive.len() > 1 && &by_archive[by_archive.len() - 1] == last {
                by_archive = &by_archive[..by_archive.len() - 1];
            } else {
                break;
            }
        }

        for a in by_archive.iter() {
            leb(&mut rng_out, a.len() as u64)?;
            let mut cursor = 0;
            for r in a {
                leb(&mut rng_out, (r.source.start - cursor) as u64)?;
                cursor = r.source.start;
                // decoding rule is: if len is 0, copy whole file
                let len = if r.source.len() == r.source_file_len {
                    0
                } else {
                    r.source.len()
                };
                leb(&mut rng_out, len as u64)?;
            }
        }
    }

    write_zstd_chunk(&mut out, 'R', &rng_out, None)?;
    drop(rng_out);

    eprintln!("compressing {} ranges", archive.file_ranges.len());
    // TODO: don't compress files that are 100% reused (no gaps)
    let mut dictionary = Vec::new();
    for file_in_chunks in &archive.file_ranges {
        // dictionary is intentionally not cleared
        // TODO: make a parallelizable scheme

        let target_path = target_root.join(&archive.file_names[file_in_chunks.target_file_name]);
        let target_file = std::fs::read(&target_path)?;
        if file_in_chunks.by_archive[0].iter().filter(|r| !r.fake_data).map(|r| r.target.len()).sum::<usize>() == target_file.len() {
            // eprintln!("assuming whole file is reused, no compression {}", target_path.display()); // FIXME: won't be true when gaps are extended
            continue;
        }

        let chunks_start = dictionary.len();

        // all archives give same data, so pick any
        for r in &file_in_chunks.by_archive[0] {
            let path = set.archives[0].root.join(&archive.file_names[r.source_file_index]);
            let chunk = load_file_range(&path, r.source.clone())?;
            dictionary.extend_from_slice(&chunk);
        }

        let written = write_zstd_chunk(&mut out, 'F', &target_file, Some(&dictionary))?;

        //WAT
        // let mut maybe_orig_file = Vec::new();
        // for r in &file_in_chunks.by_archive[0] {
        //     let path = set.archives[0].root.join(&archive.file_names[r.source_file_index]);
        //     maybe_orig_file = std::fs::read(path)?;
        //     break;
        // }

        // let mut diff = Vec::new();
        // bsdiff::diff::diff(&maybe_orig_file, &target_file, &mut diff)?;

        // let normal_zstd = zstd::encode_all(&mut target_file.as_slice(), ZSTD_LEVEL).unwrap().len();
        // let zdiff = zstd::encode_all(&mut diff.as_slice(), ZSTD_LEVEL).unwrap().len();
        eprintln!("compresssed {} ({}) to {} with {} dict", target_path.display(), target_file.len(), written, dictionary.len() - chunks_start);

        // replace speculation with decompressed file
        // for a "solid" archive
        dictionary.truncate(chunks_start);
        if dictionary.len() > 128_000 {
            dictionary = dictionary[dictionary.len() - 128_000..].to_vec();
        }
        dictionary.extend_from_slice(&target_file);
    }

    Ok(())
}

const ZSTD_LEVEL: i32 = 13;

fn write_zstd_chunk(out: &mut BufWriter<File>, name: char, data: &[u8], dictionary: Option<&[u8]>) -> Result<usize, Error> {
    let mut compr = Vec::with_capacity(data.len()/2);
    let mut e = if let Some(dictionary) = dictionary {
        let mut e = zstd::stream::write::Encoder::with_dictionary(&mut compr, ZSTD_LEVEL, &dictionary)?;
        e.long_distance_matching(true)?;
        e
    } else {
        zstd::stream::write::Encoder::new(&mut compr, ZSTD_LEVEL)?
    };

    e.write_all(data)?;
    e.finish()?;

    let name = name as u8;
    out.write_all(std::slice::from_ref(&name))?;
    leb(out, compr.len() as u64)?;
    out.write_all(&compr)?;
    Ok(compr.len())
}
